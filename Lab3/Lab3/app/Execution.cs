﻿using System;
using System.Linq;
using Lab3.data.projects;
using Lab3.data.projects.impl;
using Lab3.data.projects.models;

namespace Lab3.app
{
    public class Execution
    {
        private ProjectsRepository repository = new ProjectsRepositoryImpl();

        public void Execute()
        {
            Query1();
            Query2();
            Query3();
            Query4();
            Query5();
            Query6();
            Query7();
            Query8();
            Query9();
            Query10();
            Query11();
            Query12();
        }

        private void Query1()
        {
            Console.WriteLine("--------Query 1---------");

            var names = repository.loadAllProjects()
                .Select(p => p.Name);

            names.ToList().ForEach(item => Console.WriteLine(item));
        }

        private void Query2()
        {
            Console.WriteLine("--------Query 2---------");

            var names = repository.loadAllProjects()
                .Where(p => p.Status == ProjectStatis.InProgress)
                .Select(p => p.Name);

            foreach (var name in names)
            {
                Console.WriteLine(name);
            }
        }

        private void Query3()
        {
            Console.WriteLine("--------Query 3---------");

            var sortedEmployees = repository.loadAllEmployees()
                .Where(e => e.Position == "Android Developer")
                .OrderBy(e => e.FirstName)
                .ThenBy(e => e.LastName);

            foreach (var employee in sortedEmployees)
            {
                Console.WriteLine($"{employee.FirstName} {employee.LastName}");
            }
        }

        private void Query4()
        {
            Console.WriteLine("--------Query 4---------");

            var prjojectsByTeams = repository.loadAllProjects()
                .GroupBy(p => p.Status);

            foreach (var group in prjojectsByTeams)
            {
                Console.WriteLine(group.Key);

                foreach (var project in group)
                {
                    Console.WriteLine(project.Name);
                }
            }
        }

        private void Query5()
        {
            Console.WriteLine("--------Query 5---------");

            var fullBudget = repository.loadAllProjects().Sum(p => p.Budget);

            Console.WriteLine(fullBudget);
        }

        private void Query6()
        {
            Console.WriteLine("--------Query 6---------");

            var maxBudgetDeoneProject = repository.loadAllProjects()
                .Where(p => p.Status == ProjectStatis.Ready || p.Status == ProjectStatis.Verified)
                .Max(p => p.Budget);

            Console.WriteLine(maxBudgetDeoneProject);

        }

        private void Query7()
        {
            Console.WriteLine("--------Query 7---------");

            var result = repository.loadAllProjects()
                .Where(p => p.Status != ProjectStatis.Pending)
                .All(p => p.Budget > 10000);

            Console.WriteLine(result);

        }

        private void Query8()
        {
            Console.WriteLine("--------Query 8---------");

            var result = repository.loadAllEmployees()
                .Where(p => p.Position == "Android Developer")
                .Take(3);

            foreach (var employee in result)
            {
                Console.WriteLine(employee);
            }
        }

        private void Query9()
        {
            Console.WriteLine("--------Query 9---------");

            var result = repository.loadAllProjects()
                .SelectMany(p => p.DevelopmentTeams, (project, team) => new { PrjectName = project.Name, TeamName = team.Name } );

            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
        }

        private void Query10()
        {
            Console.WriteLine("--------Query 10---------");

            var firstTeam = repository.loadAllTeams()
                .FirstOrDefault(p => p.Members.Count > 2);

            Console.WriteLine(firstTeam.Name);

        }

        private void Query11()
        {
            Console.WriteLine("--------Query 11---------");

            var result = repository.loadAllEmployees()
                .Where(e => e.Position == "iOS Developer")
                .Count();

            Console.WriteLine(result);
        }

        private void Query12()
        {
            Console.WriteLine("--------Query 12---------");

            var result = repository.loadAllEmployees()
                .Skip(3).Take(2).Select(e => new { e.FirstName, e.LastName } );

            foreach (var item in result)
            {
                Console.WriteLine($"{item.FirstName} {item.LastName}");
            }
        }
    }
}