﻿using System;
using System.Collections.Generic;

namespace Lab3.data.projects.models
{

    public enum ProjectStatis
    {
        Pending,
        Processing,
        WaitingForStart,
        InProgress,
        InTesting,
        Ready,
        Verified
    }

    public class Project
    {

        public string Id { get; set; }
        public string Name { get; set; }
        public float Budget { get; set; }

        public string Description { get; set; }

        public ProjectStatis Status { get; set; }

        public List<Team> DevelopmentTeams { get; set; }

    }
}