﻿using System;
using System.Collections.Generic;

namespace Lab3.data.projects.models
{
    public class Team
    {
        private List<Employee> members = new List<Employee>();

        public String Name { get; set; }

        public List<Employee> Members
        {
            get { return members; }
        }

        public Team(List<Employee> members)
        {
            this.members = members;
        }
    }
}
