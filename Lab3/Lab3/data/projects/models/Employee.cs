﻿using System;
namespace Lab3.data.projects.models
{

    public class Employee
    {
        public string Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Position { get; set; }

        public string Email { get; set; }
    }
}
