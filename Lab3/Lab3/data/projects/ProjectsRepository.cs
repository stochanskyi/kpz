﻿using System;
using System.Collections.Generic;
using Lab3.data.projects.models;

namespace Lab3.data.projects
{
    public interface ProjectsRepository
    {
        List<Employee> loadAllEmployees();

        List<Project> loadAllProjects();

        List<Team> loadAllTeams();
    }
}
