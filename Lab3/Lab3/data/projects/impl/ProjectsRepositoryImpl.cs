﻿using System;
using System.Collections.Generic;
using Lab3.data.projects.models;

namespace Lab3.data.projects.impl
{
    public class ProjectsRepositoryImpl : ProjectsRepository
    {

        private static List<Employee> mockedEmployees = new List<Employee>()
            {
                new Employee { Id = "1", FirstName = "Daniel", LastName = "Martinez", Email = "dan_m@gmail.com", Position = "Manager" },
                new Employee { Id = "2", FirstName = "Miguel", LastName = "Watson", Email = "m.watson@gmail.com", Position = "Android Developer" },
                new Employee { Id = "3", FirstName = "Robert", LastName = "Carlos", Email = "robertc@gmail.com", Position = "Forntend Developer" },
                new Employee { Id = "4", FirstName = "Olivia", LastName = "Swan", Email = "olivia@gmail.com", Position = "iOS Developer" },
                new Employee { Id = "5", FirstName = "Mark", LastName = "Onil", Email = "monil@gmail.com", Position = "Bookkeeper" },
                new Employee { Id = "6", FirstName = "Tom", LastName = "Chain", Email = "t.ch@gmail.com", Position = "Business Analyst" },
                new Employee { Id = "7", FirstName = "Inna", LastName = "Romen", Email = "i.rom@gmail.com", Position = "Android Developer" },
                new Employee { Id = "8", FirstName = "David", LastName = "Marquis", Email = "dm@gmail.com", Position = "Backend Developer" },
                new Employee { Id = "9", FirstName = "Jack", LastName = "Watson", Email = "i.rom@gmail.com", Position = "Android Developer" },
                new Employee { Id = "10", FirstName = "George", LastName = "Wayne", Email = "geargew@gmail.com", Position = "iOS Developer" },
            };

        private static List<Team> mockedTeams = new List<Team>()
            {
                new Team(new List<Employee>() { mockedEmployees[1], mockedEmployees[6], mockedEmployees[8] })  { Name = "Android Team" },
                new Team(new List<Employee>() { mockedEmployees[2], mockedEmployees[7] })  { Name = "Web Team" },
                new Team(new List<Employee>() { mockedEmployees[3], mockedEmployees[9] }) { Name = "iOS Team" }
            };

        private static List<Project> mockedProjects = new List<Project>()
            {
                new Project { Id = "1", Name = "Apolo", Description = "Some description should be here, but I'm too lazy to add it", Budget = 123000, Status = ProjectStatis.InProgress, DevelopmentTeams = new List<Team>() { mockedTeams[0], mockedTeams[2] } },
                new Project { Id = "2", Name = "Jendet", Description = "Some description should be here, but I'm too lazy to add it", Budget = 203000, Status = ProjectStatis.Ready, DevelopmentTeams = new List<Team>() { mockedTeams[1], mockedTeams[2] } },
                new Project { Id = "3", Name = "Renter", Description = "Some description should be here, but I'm too lazy to add it", Budget = 90000, Status = ProjectStatis.InProgress, DevelopmentTeams = new List<Team>() { mockedTeams[1], mockedTeams[2] } },
                new Project { Id = "4", Name = "Renter", Description = "Some description should be here, but I'm too lazy to add it", Budget = 90000, Status = ProjectStatis.InProgress, DevelopmentTeams = new List<Team>() { mockedTeams[1], mockedTeams[0] } },
                new Project { Id = "5", Name = "Liskon", Description = "Some description should be here, but I'm too lazy to add it", Budget = 900000, Status = ProjectStatis.WaitingForStart, DevelopmentTeams = new List<Team>() { mockedTeams[0], mockedTeams[1] } },
                new Project { Id = "6", Name = "Moster", Description = "Some description should be here, but I'm too lazy to add it", Budget = 120000, Status = ProjectStatis.Processing, DevelopmentTeams = new List<Team>() { mockedTeams[0], mockedTeams[2], mockedTeams[1] } },
                new Project { Id = "7", Name = "Danersteco", Description = "Some description should be here, but I'm too lazy to add it", Budget = 300000, Status = ProjectStatis.Verified, DevelopmentTeams = new List<Team>() { mockedTeams[0], mockedTeams[2] } },
                new Project { Id = "8", Name = "Moresenc", Description = "Some description should be here, but I'm too lazy to add it", Budget = 120000, Status = ProjectStatis.Processing, DevelopmentTeams = new List<Team>() { mockedTeams[1] } },
                new Project { Id = "9", Name = "Persoto", Description = "Some description should be here, but I'm too lazy to add it", Budget = 390000, Status = ProjectStatis.InProgress, DevelopmentTeams = new List<Team>() { mockedTeams[0], mockedTeams[1], mockedTeams[2] } },
                new Project { Id = "10", Name = "Migeto", Description = "Some description should be here, but I'm too lazy to add it", Budget = 450000, Status = ProjectStatis.Pending, DevelopmentTeams = new List<Team>() },
                new Project { Id = "11", Name = "Rebinno", Description = "Some description should be here, but I'm too lazy to add it", Budget = 550000, Status = ProjectStatis.InProgress, DevelopmentTeams = new List<Team>() { mockedTeams[0], mockedTeams[1], mockedTeams[2] } },
                new Project { Id = "12", Name = "Migeto", Description = "Some description should be here, but I'm too lazy to add it", Budget = 450000, Status = ProjectStatis.Verified, DevelopmentTeams = new List<Team>() { mockedTeams[0] } },
                new Project { Id = "13", Name = "Tenoro", Description = "Some description should be here, but I'm too lazy to add it", Budget = 1500000, Status = ProjectStatis.InProgress, DevelopmentTeams = new List<Team>() { mockedTeams[1], mockedTeams[2] } },
            };

        public List<Employee> loadAllEmployees()
        {
            return mockedEmployees;
        }

        public List<Team> loadAllTeams()
        {
            return mockedTeams;
        }

        public List<Project> loadAllProjects()
        {
            return mockedProjects;
        }

    }
}