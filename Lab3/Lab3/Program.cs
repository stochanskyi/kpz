﻿using System;
using System.Linq;
using Lab3.app;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            new Execution().Execute();
        }
    }
}
