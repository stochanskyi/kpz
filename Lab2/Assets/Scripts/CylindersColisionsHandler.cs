﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CylindersColisionsHandler : MonoBehaviour
{
    private GameObject losingItem;

    private void Start()
    {
        Random random = new Random();

        int index = Random.Range(1, transform.childCount) - 1;

        losingItem = transform.GetChild(index).gameObject;


        ActorCollisionsManager.CollisionEvent += onCollisionWithActor;

        ActorCollisionsManager.CollisionEvent += (ob) => {  }
    }

    private void onCollisionWithActor(GameObject item)
    {
        Debug.Log(item.name);

        Material material = item.GetComponent<Renderer>().material;

        Color color;

        if (item == losingItem)
        {
            color = Color.red;
            StopGame();
        }
        else
        {
            color = Color.cyan;
            startEngineIfGreen(item);
        }

        material.color = color;
    }

    private void StopGame()
    {
        ActorCollisionsManager.CollisionEvent -= onCollisionWithActor;

    }

    private void startEngineIfGreen(GameObject item)
    {
        var rigitbody = SetRigitbody(item);

        rigitbody.AddForce(0f, 100f, 0f);
    }

    private Rigidbody SetRigitbody(GameObject item)
    {
        if (item.GetComponent<Rigidbody>() != null)
        {
            return item.GetComponent<Rigidbody>();
        }

        var rigidbody = item.AddComponent<Rigidbody>();
        rigidbody.useGravity = false;

        return rigidbody;
    }
}