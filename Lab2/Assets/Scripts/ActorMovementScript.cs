﻿using UnityEngine;
using UnityEngine.InputSystem;

public class ActorMovementScript : MonoBehaviour
{

    private const float MOVEMENT_SPEED = 30f;

    private Rigidbody rigidbody;

    private Vector3 pendingMovement;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    private void OnMove(InputValue inputValue)
    {
        var values = inputValue.Get<Vector2>();
        pendingMovement = new Vector3(values.x, 0.0f, values.y);
    }

    private void FixedUpdate()
    {
        rigidbody.AddForce(pendingMovement * MOVEMENT_SPEED);
    }

}