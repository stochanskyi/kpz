﻿using System;
using UnityEngine;

public class ActorCollisionsManager : MonoBehaviour
{

    private const string COLLECTABLE_TAG = "Collectables";

    public delegate void CollisionHandler(GameObject item);
    public static event CollisionHandler CollisionEvent;

    //public static event Action<GameObject> CollisionEventAlternative;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == COLLECTABLE_TAG)
        {
            CollisionEvent?.Invoke(collision.gameObject);
        }
    }
}