﻿using System;
using IT_management.users.models;
using IT_management.users.models.enums;

namespace IT_management
{
    class A
    {
        public static string asda = "asd";
    }

    class B : A
    {
        private string a = asda;
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var employee = new Employee("1", "David", "Stone", "dstone@gmail.com", Position.AndroidDev);

            var a = 1;
            object ob = a;
            processObject(ob);

            var x = 0;
            var y = 0;
            testFunction(ref x, out y);
        }

        private static void testFunction(ref int param1, out int param2)
        {
            var result1 = 3;
            var result2 = 35;

            param1 = 3;
            param2 = result2;
        }
        
        private static void processObject(object ob)
        {
            if (ob is int)
            {
                var i = (int) ob;
                Console.Write(i);
            }
        }
    }
}