using System.Collections.Generic;
using System.Linq;
using IT_management.users.models;

namespace IT_management.users
{
    public class UsersRepository : IUsersRepository
    {
        private IList<User> Users;

        public IList<User> getUsers() => Users;

        public IList<User> getUsers(IList<string> ids) => Users.Where((user => ids.Any(id => id == user.Id))).ToList();
        
        public Customer getCustomer(string id)
        {
            var customer = Users.FirstOrDefault(user => user.Id == id);
            return customer as Customer;
        }

        public void addCustomer(Customer customer)
        {
            Users.Add(customer);
        }
    }
}