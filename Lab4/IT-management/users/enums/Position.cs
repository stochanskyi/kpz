namespace IT_management.users.models.enums
{
    public enum Position
    {
        AndroidDev,
        IosDev,
        Frontend,
        Backend,
        UIUX,
        PM,
        QA
    }
}