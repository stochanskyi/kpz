using System.Collections.Generic;
using IT_management.users.models;
using IT_management.users.models.enums;

namespace IT_management.users
{
    public interface IUsersRepository
    {

        IList<User> getUsers();

        IList<User> getUsers(IList<string> ids);

        Customer getCustomer(string id);

        void addCustomer(Customer customer);
        
        interface IEmployee
        {
            Position Position { get; }
        }

        interface ICustomer
        {
            string Country { get; }
        }
    }
}