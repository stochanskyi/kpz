using System;

namespace IT_management.users.models
{
    public abstract class User
    {
        protected User(string id, string name, string surname)
        {
            this.id = id;
            Name = name;
            Surname = surname;
        }
        protected User(string id, string name, string surname, string email) : this(id, name, surname)
        {
            Email = email;
        }
        protected string id = null;

        public string Id
        {
            get
            {
                if (id == null) throw new Exception("User withour id");
                return id;
            }
        }

        public string Name { get; }
        public string Surname { get; }
        public string Email { get; }

    }
}