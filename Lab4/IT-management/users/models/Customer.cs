using System;

namespace IT_management.users.models
{
    public class Customer : User, IUsersRepository.ICustomer
    {
        public Customer(
            string id,
            string name,
            string surname,
            string country,
            string phoneNumber) : this(id, name, surname, "", country, phoneNumber)
        {
        }
        public Customer(
            string id,
            string name,
            string surname,
            string email,
            string country,
            string phoneNumber) : base(id, name, surname, email)
        {
            this.country = country;
            PhoneNumber = phoneNumber;
        }

        string country;
        public string Country => country;
        public string PhoneNumber { get; }
        
        public ContactInfo Info { get; set; }
        
        public class ContactInfo
        {
            public string InstaLink { get; set; }
            public string FacebookLink { get; set; }
            public string LinkedInLink { get; set; }
        }
    }

}