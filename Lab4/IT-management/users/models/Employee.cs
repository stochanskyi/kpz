using System.Runtime.CompilerServices;
using IT_management.users.models.enums;

namespace IT_management.users.models
{
    class Employee : User, IUsersRepository.IEmployee
    {

        public Employee(string id, string name, string surname, string email, Position position) : base(id, name, surname, email)
        {
            this.position = position;
        }

        private Position position;
        
        public Position Position => position;

        public void doSomething()
        {
            var someId = id;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Employee)) return false;

            var employee2 = (Employee) obj;

            return id == employee2.id &&
                   Name == employee2.Name && 
                   Surname == employee2.Surname && 
                   Email == employee2.Email &&
                   position == employee2.position;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode() + 1;
        }

        public override string ToString()
        {
            return $"{Name} {Surname} {Email}";
        }
    }
}